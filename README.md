# This is the main repository of the Sufflain project.

It contains everything necessary to build the app.

## Docker Compose demo
[![asciicast](https://asciinema.org/a/TXDnguakkXI5l67HXhCPS11FF.svg)](https://asciinema.org/a/TXDnguakkXI5l67HXhCPS11FF?speed=1.5)

## Cloning the repo
```bash
git clone --recurse-submodules https://gitlab.com/Sufflain/main.git
```

## Project configuration
1. [server](https://gitlab.com/Sufflain/server#project-configuration)
2. [web-server](https://gitlab.com/Sufflain/web-server#project-configuration)

## Build services
```bash
docker-compose build
```

## Usage
### Run services
```bash
docker-compose run -d
```

### View logs
```bash
docker-compose logs {web-,}server
```

### Stop services
```bash
docker-compose stop {web-,}server
```

## Commit Message Guidelines
We use [Conventional Commits](https://www.conventionalcommits.org/en/v1.0.0/) to format our commit
messages.
